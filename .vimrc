set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" " alternatively, pass a path where Vundle should install plugins
" "call vundle#begin('~/some/path/here')
"
" " let Vundle manage Vundle, required
" Plugin 'gmarik/Vundle.vim'
"
" " The following are examples of different formats supported.
" " Keep Plugin commands between vundle#begin/end.
" " plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" " plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" " Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" " git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" " The sparkup vim script is in a subdirectory of this repo called vim.
" " Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" " Avoid a name conflict with L9
" Plugin 'user/L9', {'name': 'newL9'}
Plugin 'fatih/vim-go'
Plugin 'bling/vim-airline'
Plugin 'derekwyatt/vim-scala'
Plugin 'Valloric/YouCompleteMe'
" " All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
"----------------------------------------------------------------------          |~
" Basic Options                                                                  |~
"----------------------------------------------------------------------          |~
let mapleader=";"         " The <leader> key                                     |~
set autoread              " Reload files that have not been modified             |~
set backspace=2           " Makes backspace not behave all retarded-like         |~
set cursorline            " Highlight the line the cursor is on                  |~
set hidden                " Allow buffers to be backgrounded without being saved |~
set laststatus=2          " Always show the status bar                           |~
set list                  " Show invisible characters                            |~
set listchars=tab:›\ ,eol:¬,trail:⋅ "Set the characters for the invisibles       |~
set relativenumber        " Show relative line numbers                           |~
set ruler                 " Show the line number and column in the status bar    |~
set t_Co=256              " Use 256 colors                                       |~
set scrolloff=999         " Keep the cursor centered in the screen               |~
set showmatch             " Highlight matching braces                            |~
set showmode              " Show the current mode on the open buffer             |~
set splitbelow            " Splits show up below by default                      |~
set splitright            " Splits go to the right by default                    |~
set title                 " Set the title for gvim                               |~
set visualbell            " Use a visual bell to notify us       
 
if !has("win32")
    set showbreak=↪       " The character to put to show a line has been wrapped
end
 
syntax on                 " Enable filetype detection by syntax
 
" Search settings
set hlsearch   " Highlight results
set ignorecase " Ignore casing of searches
set incsearch  " Start showing results as you type
set smartcase  " Be smart about case sensitivity when searching
 
" Tab settings
set expandtab     " Expand tabs to the proper type and size
set tabstop=4     " Tabs width in spaces
set softtabstop=4 " Soft tab width in spaces
set shiftwidth=4  " Amount of spaces when shifting
 
" Tab completion settings
 
" Shortcut to yanking to the system clipboard
map <leader>y "*y
map <leader>p "*p
" JSON
let g:vim_json_syntax_conceal = 0
 
" Powerline
let g:Powerline_symbols="fancy" " Fancy styling

set rtp+=$GOROOT/misc/vim
filetype plugin indent on
syntax on 
colorscheme molokai
"let g:solarized_termcolors=256
"syntax enable
"set background=light
"colorscheme solarized
